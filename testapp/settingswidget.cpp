/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtGui module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QVariant>
#include <QSlider>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QGroupBox>
#include <QToolButton>
#include <QCheckBox>
#include <QScrollArea>

#include "settingswidget.h"
#include "qkineticscroller.h"

struct MetricItem
{
    QKineticScroller::ScrollMetric metric;
    const char *name;
    int scaling;
    const char *unit;
    QVariant min, max;
    QVariant step;
};

class MetricItemUpdater : public QObject
{
    Q_OBJECT
public:
    MetricItemUpdater(MetricItem *item)
        : m_item(item), m_scroller(0)
    {
        if (m_item->min.type() == QVariant::PointF) {
            m_slider = new QSlider(Qt::Horizontal);
            m_slider->setSingleStep(1);
            m_slider->setMinimum(-1);
            m_slider->setMaximum(int((m_item->max.toPointF().x() - m_item->min.toPointF().x()) / m_item->step.toReal()));
            m_slider->setValue(-1);
            m_slider2 = new QSlider(Qt::Horizontal);
            m_slider2->setSingleStep(1);
            m_slider2->setMinimum(-1);
            m_slider2->setMaximum(int((m_item->max.toPointF().y() - m_item->min.toPointF().y()) / m_item->step.toReal()));
            m_slider2->setValue(-1);
            m_control = new QWidget();
            QHBoxLayout *lay = new QHBoxLayout(m_control);
            lay->setContentsMargins(0, 0, 0, 0);
            lay->addWidget(m_slider);
            lay->addWidget(m_slider2);
        } else {
            m_slider = new QSlider(Qt::Horizontal);
            m_slider->setSingleStep(1);
            m_slider->setMinimum(-1);
            m_slider->setMaximum(int((m_item->max.toReal() - m_item->min.toReal()) / m_item->step.toReal()));
            m_slider->setValue(-1);
            m_slider2 = 0;
            m_control = m_slider;
        }
        m_valueLabel = new QLabel();
        m_nameLabel = new QLabel(QLatin1String(m_item->name));
        if (m_item->unit && m_item->unit[0])
            m_nameLabel->setText(m_nameLabel->text() + QLatin1String("   [") + QLatin1String(m_item->unit) + QLatin1String("]"));
        m_resetButton = new QToolButton();
        m_resetButton->setText(QLatin1String("Reset"));
        m_resetButton->setEnabled(false);

        connect(m_resetButton, SIGNAL(clicked()), this, SLOT(reset()));
        connect(m_slider, SIGNAL(valueChanged(int)), this, SLOT(sliderChanged(int)));
        if (m_slider2)
            connect(m_slider2, SIGNAL(valueChanged(int)), this, SLOT(sliderChanged(int)));

        m_slider->setMinimum(0);
        if (m_slider2)
            m_slider2->setMinimum(0);
    }

    void setKineticScroller(QKineticScroller *scroller)
    {
        m_scroller = scroller;
        m_slider->setEnabled(scroller);
        if (m_slider2)
            m_slider2->setEnabled(scroller);
        m_nameLabel->setEnabled(scroller);
        m_valueLabel->setEnabled(scroller);
        m_resetButton->setEnabled(scroller);

        if (!m_scroller)
            return;

        m_default_value = m_scroller->scrollMetric(m_item->metric);
        valueChanged(m_default_value);
    }

    QWidget *nameLabel()    { return m_nameLabel; }
    QWidget *valueLabel()   { return m_valueLabel; }
    QWidget *valueControl() { return m_control; }
    QWidget *resetButton()  { return m_resetButton; }

private slots:
    void valueChanged(const QVariant &v)
    {
        m_value = v;
        switch (m_item->min.type()) {
        case QMetaType::Float:
        case QVariant::Double: {
            m_slider->setValue(int((m_value.toReal() * m_item->scaling - m_item->min.toReal()) / m_item->step.toReal()));
            break;
        }
        case QVariant::Int: {
            m_slider->setValue(int((m_value.toInt() * m_item->scaling - m_item->min.toInt()) / m_item->step.toInt()));
            break;
        }
        case QVariant::PointF: {
            m_slider->setValue(int((m_value.toPointF().x() * m_item->scaling - m_item->min.toPointF().x()) / m_item->step.toReal()));
            m_slider2->setValue(int((m_value.toPointF().y() * m_item->scaling - m_item->min.toPointF().y()) / m_item->step.toReal()));
            break;
        }
        default: break;
        }
    }

    void sliderChanged(int value)
    {
        bool second = (m_slider2 && (sender() == m_slider2));
        QString text;

        switch (m_item->min.type()) {
        case QMetaType::Float:
        case QVariant::Double: {
            qreal d = m_item->min.toReal() + qreal(value) * m_item->step.toReal();
            text = QString::number(d);
            m_value = d / qreal(m_item->scaling);
            break;
        }
        case QVariant::Int: {
            int i = m_item->min.toInt() + int(qreal(value) * m_item->step.toReal());
            text = QString::number(i);
            m_value = i / m_item->scaling;
            break;
        }
        case QVariant::PointF: {
            qreal p = (second ? m_item->min.toPointF().y() : m_item->min.toPointF().x()) + qreal(value) * m_item->step.toReal();
            text = QString("%1, %2").arg(second ? m_value.toPointF().x() * m_item->scaling : p).arg(second ? p : m_value.toPointF().y() * m_item->scaling);
            m_value = QPointF(second ? m_value.toPointF().x() : p / m_item->scaling, second ? p / m_item->scaling : m_value.toPointF().y());
            break;
        }
        default: break;
        }
        m_valueLabel->setText(text);
        if (m_scroller)
            m_scroller->setScrollMetric(m_item->metric, m_value);

        m_resetButton->setEnabled(m_value != m_default_value);
    }

    void reset()
    {
        if (m_scroller)
            m_scroller->setScrollMetric(m_item->metric, m_value);
        valueChanged(m_default_value);
    }

private:
    MetricItem *m_item;
    QKineticScroller *m_scroller;

    QSlider *m_slider, *m_slider2;
    QLabel *m_nameLabel, *m_valueLabel;
    QToolButton *m_resetButton;
    QWidget *m_control;

    QVariant m_value, m_default_value;
};

#define METRIC(x) QKineticScroller::x, #x

MetricItem items[QKineticScroller::ScrollMetricCount] = {
    { METRIC(DragVelocitySmoothingFactor),    1,    "",         qreal(0), qreal(1), qreal(0.01) },

    { METRIC(LinearDecelerationFactor),       1,    "m/s\xb2",  qreal(0), qreal(3), qreal(0.01) },
    { METRIC(ExponentialDecelerationBase),    1,    "",         qreal(0), qreal(1), qreal(0.01) },
    { METRIC(OvershootSpringConstant),        1,    "kg/s\xb2", qreal(1), qreal(500), qreal(1)  },
    { METRIC(OvershootDragResistanceFactor),  1,    "",         qreal(0), qreal(1), qreal(0.01) },
    { METRIC(OvershootMaximumDistance),       1000, "mm, mm",   QPointF(0, 0), QPointF(500, 500), qreal(1) },

    { METRIC(DragStartDistance),              1000, "mm",       qreal(1), qreal(20), qreal(0.1) },
    { METRIC(DragStartDirectionErrorMargin),  1000, "mm",       qreal(1), qreal(20), qreal(0.1) },

    { METRIC(MinimumVelocity),                1,    "m/s",      qreal(0), qreal(7), qreal(0.01) },
    { METRIC(MaximumVelocity),                1,    "m/s",      qreal(0), qreal(7), qreal(0.01) },
    { METRIC(MaximumNonAcceleratedVelocity),  1,    "m/s",      qreal(0), qreal(7), qreal(0.01) },

    { METRIC(MaximumClickThroughVelocity),    1,    "m/s",      qreal(0), qreal(7), qreal(0.01) },
    { METRIC(AxisLockThreshold),              1,    "",         qreal(0), qreal(1), qreal(0.01) },

    { METRIC(FramesPerSecond),                1,    "frames/s", int(10), int(100), int(1) },

    { METRIC(FastSwipeMaximumTime),           1000, "ms",       qreal(10), qreal(1000), qreal(1) },
    { METRIC(FastSwipeMinimumVelocity),       1,    "m/s",      qreal(0), qreal(7), qreal(0.01) },
    { METRIC(FastSwipeBaseVelocity),          1,    "m/s",      qreal(0), qreal(7), qreal(0.01) }
};

#undef METRIC

void SettingsWidget::addToGrid(QGridLayout *grid, QWidget *label, int widgetCount, ...)
{
    va_list args;
    va_start(args, widgetCount);

    int rows = grid->rowCount();
    int cols = grid->columnCount();

    if (label) {
        if (m_smallscreen)
            grid->addWidget(label, rows++, 0, 1, qMax(cols, widgetCount));
        else
            grid->addWidget(label, rows, 0);
    }
    for (int i = 0; i < widgetCount; i++) {
        if (QWidget *w = va_arg(args, QWidget *))
            grid->addWidget(w, rows, m_smallscreen ? i : i + 1);
    }
    va_end(args);
}

class HackScroller : public QKineticScroller {
public:
    using QKineticScroller::viewportSize;
    using QKineticScroller::maximumContentPosition;
};

class HackSpinBox : public QSpinBox {
public:
    using QAbstractSpinBox::lineEdit;
};

SettingsWidget::SettingsWidget(bool smallscreen)
    : QScrollArea(), m_scroller(0), m_smallscreen(smallscreen)
{
    setWindowTitle(QLatin1String("Settings"));
    QWidget *view = new QWidget();
    QVBoxLayout *layout = new QVBoxLayout(view);
    QGroupBox *grp;
    QGridLayout *grid;

    grp = new QGroupBox(QLatin1String("General"));
    grid = new QGridLayout();
    grid->setVerticalSpacing(smallscreen ? 4 : 2);
    m_onoff = new QCheckBox(QLatin1String("Enabled"));
    connect(m_onoff, SIGNAL(toggled(bool)), this, SLOT(enabledChanged(bool)));
    addToGrid(grid, m_onoff, 0);

    m_dpi = new QSpinBox();
    m_dpi->setRange(10, 1000);
    m_dpi->setSuffix(QLatin1String(" dpi"));
    connect(m_dpi, SIGNAL(valueChanged(int)), this, SLOT(dpiChanged(int)));
    addToGrid(grid, new QLabel("DPI"), 1, m_dpi);

    m_hospolicy = new QComboBox();
    m_hospolicy->addItem("When Scrollable", QKineticScroller::OvershootWhenScrollable);
    m_hospolicy->addItem("Always On",       QKineticScroller::OvershootAlwaysOn);
    m_hospolicy->addItem("Always Off",      QKineticScroller::OvershootAlwaysOff);
    connect(m_hospolicy, SIGNAL(currentIndexChanged(int)), this, SLOT(overshootPolicyChanged(int)));
    addToGrid(grid, new QLabel("Horizontal Overshoot Policy"), 1, m_hospolicy);

    m_vospolicy = new QComboBox();
    m_vospolicy->addItem("When Scrollable", QKineticScroller::OvershootWhenScrollable);
    m_vospolicy->addItem("Always On",       QKineticScroller::OvershootAlwaysOn);
    m_vospolicy->addItem("Always Off",      QKineticScroller::OvershootAlwaysOff);
    connect(m_vospolicy, SIGNAL(currentIndexChanged(int)), this, SLOT(overshootPolicyChanged(int)));
    addToGrid(grid, new QLabel("Vertical Overshoot Policy"), 1, m_vospolicy);

    grp->setLayout(grid);
    layout->addWidget(grp);

    grp = new QGroupBox(QLatin1String("Scroll Metrics"));
    grid = new QGridLayout();
    grid->setVerticalSpacing(m_smallscreen ? 4 : 2);

    for (int i = 0; i < QKineticScroller::ScrollMetricCount; i++) {
        MetricItemUpdater *u = new MetricItemUpdater(items + i);
        u->setParent(this);
        addToGrid(grid, u->nameLabel(), 3, u->valueControl(), u->valueLabel(), u->resetButton());
        m_metrics.append(u);
    }
    grp->setLayout(grid);
    layout->addWidget(grp);

    grp = new QGroupBox(QLatin1String("Scroll To"));
    grid = new QGridLayout();
    grid->setVerticalSpacing(m_smallscreen ? 4 : 2);

    m_scrollx = new QSpinBox();
    m_scrolly = new QSpinBox();
    m_scrolltime = new QSpinBox();
    m_scrolltime->setRange(0, 10000);
    m_scrolltime->setValue(1000);
    m_scrolltime->setSuffix(QLatin1String(" ms"));
    QPushButton *go = new QPushButton(QLatin1String("Go"));
    connect(go, SIGNAL(clicked()), this, SLOT(scrollTo()));
    connect(m_scrollx, SIGNAL(editingFinished()), this, SLOT(scrollTo()));
    connect(m_scrolly, SIGNAL(editingFinished()), this, SLOT(scrollTo()));
    connect(m_scrolltime, SIGNAL(editingFinished()), this, SLOT(scrollTo()));
    grid->addWidget(new QLabel(QLatin1String("X:")), 0, 0);
    grid->addWidget(m_scrollx, 0, 1);
    grid->addWidget(new QLabel(QLatin1String("Y:")), 0, 2);
    grid->addWidget(m_scrolly, 0, 3);
    int row = smallscreen ? 1 : 0;
    int col = smallscreen ? 0 : 4;
    grid->addWidget(new QLabel(QLatin1String("in")), row, col++);
    grid->addWidget(m_scrolltime, row, col++);
    if (smallscreen) {
        grid->addWidget(go, row, col + 1);
    } else {
        grid->addWidget(go, row, col);
        grid->setColumnStretch(5, 1);
        grid->setColumnStretch(6, 1);
    }
    grid->setColumnStretch(1, 1);
    grid->setColumnStretch(3, 1);
    grp->setLayout(grid);
    layout->addWidget(grp);
    layout->addStretch(100);

    setWidget(view);
    setWidgetResizable(true);
}

void SettingsWidget::setKineticScroller(QKineticScroller *scroller)
{
    m_scroller = scroller;
    //setEnabled(m_scroller);

    QMutableListIterator<MetricItemUpdater *> it(m_metrics);
    while (it.hasNext())
        it.next()->setKineticScroller(m_scroller);

    if (!m_scroller)
        return;

    m_onoff->setChecked(m_scroller->isEnabled());
    m_dpi->setValue(m_scroller->dpi());
    m_hospolicy->setCurrentIndex(m_hospolicy->findData(m_scroller->horizontalOvershootPolicy()));
    m_vospolicy->setCurrentIndex(m_vospolicy->findData(m_scroller->verticalOvershootPolicy()));


    QSizeF vp = static_cast<HackScroller *>(m_scroller)->viewportSize();
    QPointF maxc = static_cast<HackScroller *>(m_scroller)->maximumContentPosition();

    m_scrollx->setRange(int(-vp.width()), int(maxc.x() + vp.width()));
    m_scrolly->setRange(int(-vp.height()), int(maxc.y() + vp.height()));
}

void SettingsWidget::enabledChanged(bool on)
{
    if (m_scroller)
        m_scroller->setEnabled(on);
}

void SettingsWidget::dpiChanged(int dpi)
{
    if (m_scroller)
        m_scroller->setDpi(dpi);
}

void SettingsWidget::overshootPolicyChanged(int index)
{
    if (sender() == m_hospolicy && m_scroller)
        m_scroller->setHorizontalOvershootPolicy(static_cast<QKineticScroller::OvershootPolicy>(m_hospolicy->itemData(index).toInt()));
    else if (sender() == m_vospolicy && m_scroller)
        m_scroller->setVerticalOvershootPolicy(static_cast<QKineticScroller::OvershootPolicy>(m_hospolicy->itemData(index).toInt()));
}

void SettingsWidget::scrollTo()
{
    if ((sender() == m_scrollx) && !m_scrollx->hasFocus())
        return;
    if ((sender() == m_scrolly) && !m_scrolly->hasFocus())
        return;
    if ((sender() == m_scrolltime) && !m_scrolltime->hasFocus())
        return;
    if (!m_scroller)
        return;
    m_scroller->scrollTo(QPointF(m_scrollx->value(), m_scrolly->value()), m_scrolltime->value());
}

#include "settingswidget.moc"
