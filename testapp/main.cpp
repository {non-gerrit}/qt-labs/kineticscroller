
#include <QApplication>
#include <QListWidget>
#include <QListWidgetItem>
#include <QSplitter>
#include <QStackedWidget>
#include <QSignalMapper>
#include <QMainWindow>
#include <QMenuBar>
#include <QActionGroup>
#include <QWebView>
#include <QTimer>

#ifdef Q_WS_MAEMO_5
#  include <QAbstractKineticScroller>
#endif

#include "qscrollareakineticscroller.h"
#include "qwebviewkineticscroller.h"

#include "settingswidget.h"
#include "plotwidget.h"

bool qt_sendSpontaneousEvent(QObject *receiver, QEvent *event)
{
    return QCoreApplication::sendSpontaneousEvent(receiver, event);
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(bool smallscreen)
        : QMainWindow()
    {
        m_list = new QListWidget();
        m_list_scroller = installKineticScroller(m_list);

        for (int i = 0; i < 1000; ++i)
            new QListWidgetItem(QString("This is a test text %1 %2").arg(i).arg(QString("--------").left(i % 8)), m_list);

        m_web = new QWebView();
        m_web_scroller = installKineticScroller(m_web);

        QTimer::singleShot(1000, this, SLOT(loadUrl()));

        m_settings = new SettingsWidget(smallscreen);
        m_plot = new PlotWidget(smallscreen);

        QStackedWidget *stack = new QStackedWidget();
        stack->addWidget(m_list);
        stack->addWidget(m_web);

        QActionGroup *pages = new QActionGroup(this);
        pages->setExclusive(true);
        QSignalMapper *mapper = new QSignalMapper(this);
        connect(mapper, SIGNAL(mapped(int)), stack, SLOT(setCurrentIndex(int)));

        createAction("List", pages, mapper, 0, true);
        createAction("Web",  pages, mapper, 1);

        if (smallscreen) {
            stack->addWidget(m_settings);
            stack->addWidget(m_plot);

            createAction("Settings", pages, mapper, 2);
            createAction("Plot",     pages, mapper, 3);

            setCentralWidget(stack);
        } else {
            QSplitter *split = new QSplitter();
            m_settings->setMinimumWidth(m_settings->sizeHint().width());
            split->addWidget(stack);
            split->addWidget(m_settings);
            split->addWidget(m_plot);
            setCentralWidget(split);
        }
        menuBar()->addMenu(QLatin1String("Pages"))->addActions(pages->actions());
        connect(stack, SIGNAL(currentChanged(int)), this, SLOT(pageChanged(int)));
        pageChanged(0);
    }

private slots:
    void pageChanged(int page)
    {
        if (page < 0 || page > 1)
            return;
        switch (page) {
        case 0:
            m_settings->setKineticScroller(m_list_scroller);
            m_plot->setKineticScroller(m_list_scroller);
            break;
        case 1:
            m_settings->setKineticScroller(m_web_scroller);
            m_plot->setKineticScroller(m_web_scroller);
            break;
        default:
            break;
        }
    }

    void loadUrl()
    {
        m_web->load(QUrl("http://www.heise.de"));
    }

private:
    QAction *createAction(const char *text, QActionGroup *group, QSignalMapper *mapper, int mapping, bool checked = false)
    {
        QAction *a = new QAction(QLatin1String(text), group);
        a->setCheckable(true);
        a->setChecked(checked);
#if defined(Q_WS_MAC)
        a->setMenuRole(QAction::NoRole);
#endif
        mapper->setMapping(a, mapping);
        connect(a, SIGNAL(toggled(bool)), mapper, SLOT(map()));
        return a;
    }

    QKineticScroller *installKineticScroller(QWidget *w)
    {
#if defined(Q_WS_MAEMO_5)
        // remove the old kinetic scroller if any
        QAbstractKineticScroller *oldScroller = w->property("kineticScroller").value<QAbstractKineticScroller *>();
        oldScroller->setEnabled(false);
#endif
        // set a new kinetic scroller
        if (QAbstractScrollArea *area = qobject_cast<QAbstractScrollArea *>(w)) {
            QScrollAreaKineticScroller *newScroller = new QScrollAreaKineticScroller();
            newScroller->setWidget(area);
            return newScroller;
        } else if (QWebView *web = qobject_cast<QWebView *>(w)) {
            QWebViewKineticScroller *newScroller = new QWebViewKineticScroller();
            newScroller->setWidget(web);
            return newScroller;
        } else {
            return 0;
        }
    }

private:
    QListWidget *m_list;
    QWebView *m_web;
    QKineticScroller *m_list_scroller, *m_web_scroller;
    SettingsWidget *m_settings;
    PlotWidget *m_plot;
};

int main(int argc, char **argv)
{
    QApplication a(argc, argv);

#if defined(Q_WS_MAEMO_5) || defined(Q_WS_S60) || defined(Q_WS_WINCE)
    bool smallscreen = true;
#else
    bool smallscreen = false;
#endif

    if (a.arguments().contains(QLatin1String("--small")))
        smallscreen = true;

    MainWindow *mw = new MainWindow(smallscreen);
    if (smallscreen)
        mw->showMaximized();
    else
        mw->show();
#if defined(Q_WS_MAC)
    mw->raise();
#endif

    return a.exec();
}

#include "main.moc"
